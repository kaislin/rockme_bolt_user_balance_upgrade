package main

import (
	"rockme_bolt_user_balance_upgrade/db"
	"rockme_bolt_user_balance_upgrade/pkg/config"
	"rockme_bolt_user_balance_upgrade/pkg/utl"
	"time"
)

func init() {
	db.InitializesMongo()
}

func main() {
	config.Lgentry.Infof("check remittance - all user check Start!")
	// get all user address
	userAddresses := utl.GetUserAddress()
	// check no duplicate item
	userAddresses = utl.RemoveDuplicateElement(userAddresses)

	// get old bolt balance
	for _, item := range userAddresses {
		time.Sleep(100 * time.Millisecond)
		utl.CheckRemittance(item, utl.GetBalance(item, "OLD"))
	}
	config.Lgentry.Infof("check remittance - all user is check done!")

	config.Lgentry.Infof("check remittance - check vendor Start!")
	// get exist vendorID
	vendors, _ := utl.GetVendorIDsAndBalance()
	for _, i := range vendors {
		time.Sleep(100 * time.Millisecond)
		utl.CheckRemittance(i.NewAddress, utl.GetBalance(i.OldAddress, "OLD"))
	}
	config.Lgentry.Infof("check remittance - check vendor is check done!")

}
