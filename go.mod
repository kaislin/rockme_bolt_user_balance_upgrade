module rockme_bolt_user_balance_upgrade

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/viper v1.3.1
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
)
