package db

import mgo "github.com/globalsign/mgo"

type DBTableOp interface {
	Insert(database string, query interface{}) error
	FindAll(database string, query interface{}, data interface{}) error
	FindOne(database string, query interface{}, data interface{}) error
	PipeAll(database string, query interface{}, data interface{}) error
	PipeOne(database string, query interface{}, data interface{}) error
}

type DBTable struct {
	MyDB *mgo.Database
}

func (d *DBTable) Insert(database string, query interface{}) error {
	err := d.MyDB.C(database).Insert(query)
	return err
}

func (d *DBTable) FindAll(database string, query interface{}, data interface{}) error {
	err := d.MyDB.C(database).Find(query).All(data)
	return err
}

func (d *DBTable) FindOne(database string, query interface{}, data interface{}) error {
	err := d.MyDB.C(database).Find(query).One(data)
	return err
}

func (d *DBTable) PipeAll(database string, query interface{}, data interface{}) error {
	err := d.MyDB.C(database).Pipe(query).All(data)
	return err
}

func (d *DBTable) PipeOne(database string, query interface{}, data interface{}) error {
	err := d.MyDB.C(database).Pipe(query).One(data)
	return err
}
