package model

import (
	"github.com/globalsign/mgo/bson"
)

const COIN = "TWX"

type CoinList struct {
	CoinListID  bson.ObjectId `bson:"_id" json:"_id"`
	DeviceID    bson.ObjectId `bson:"device_id" json:"device_id"`
	ADSourceID  bson.ObjectId `bson:"ad_source_id" json:"ad_source_id"`
	TicketID    bson.ObjectId `bson:"ticket_id"`
	ADCoin      float64       `bson:"ad_coin" json:"ad_coin"`
	Status      string        `bson:"status" json:"status"`
	RedeemID    bson.ObjectId `bson:"redeem_id" json:"redeem_id"`
	UserAddress string        `bson:"user_address" json:"user_address"`
	ExchangeID  bson.ObjectId `bson:"exchange_id" json:"exchange_id"`
	Ctime       int64         `bson:"ctime" json:"ctime"`
	Utime       int64         `bson:"utime" json:"utime"`
}

type VendorList struct {
	VendorID      bson.ObjectId `bson:"_id"`
	VendorName    string        `bson:"vendor_name"`
	VendorAddress string        `bson:"vendor_address"`
	VendorStatus  string        `bson:"vendor_status"`
	VendorADList  []string      `bson:"vendor_ad_list"`
}

type TransactionLogs struct {
	ID              bson.ObjectId `bson:"_id"`
	Type            string        `bson:"type" json:"type"`
	FromAddr        string        `bson:"from_addr" json:"from_addr"`
	ToAddr          string        `bson:"to_addr" json:"to_addr"`
	ADcoin          float64       `bson:"ad_coin" json:"ad_coin"`
	GoodID          bson.ObjectId `bson:"goods_id" json:"goods_id"`
	LightTXHash     string        `bson:"light_tx_hash" json:"light_tx_hash"`
	ADSourceID      bson.ObjectId `bson:"ad_source_id" json:"ad_source_id"`
	RedeemCode      string        `bson:"redeem_code" json:"redeem_code"`
	TransactionTime int64         `bson:"transaction_time" json:"transaction_time"`
}

type StoreList struct {
	Address string `bson:"address"`
}

type RedeemCodeData struct {
	ID               bson.ObjectId `bson:"_id"`
	TicketID         bson.ObjectId `bson:"ticket_id"`
	RedeemCode       string        `bson:"redeem_code"`
	ADcoin           float64       `bson:"ad_coin"`
	ADSourceID       bson.ObjectId `bson:"ad_source_id"`
	VendorAddr       string        `bson:"vendor_address"`
	CreateTime       int64         `bson:"create_time"`
	RedeemTime       int64         `bson:"redeem_time"`
	RedeemAddr       string        `bson:"redeem_address"`
	RedeemExpireTime int64         `bson:"redeem_expire_time"`
	// Location         []float64     `bson:"location" json:"location"`
}

type VendorAddr struct {
	VendorName string `json:"vendor_name"`
	OldAddress string `json:"old_address"`
	NewAddress string `json:"new_address"`
	Balance    string `json:"balance"`
}
