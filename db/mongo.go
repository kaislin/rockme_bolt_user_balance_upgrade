package db

import (
	"rockme_bolt_user_balance_upgrade/pkg/config"
	"time"

	mgo "github.com/globalsign/mgo"
	"github.com/spf13/viper"
)

var session *mgo.Session
var database *mgo.Database

// InitializesMongo - init mongo connect
func InitializesMongo() (session *mgo.Session) {
	info := &mgo.DialInfo{
		Addrs:    []string{viper.GetString("db.host") + ":" + viper.GetString("db.port")},
		Timeout:  60 * time.Second,
		Database: viper.GetString("db.db"),
		Username: viper.GetString("db.user"),
		Password: viper.GetString("db.pwd"),
	}

	var sessionErr error
	session, sessionErr = mgo.DialWithInfo(info)
	if sessionErr != nil {
		config.Lgentry.Errorln("InitialiseMongo() Error")
		panic(sessionErr)
	}

	database = session.DB(viper.GetString("db.hosts"))
	config.Lgentry.Info("init mongo success")
	return
}

// GetMgo - get mongodb *mgo.Session
func GetMgo() *mgo.Session {
	return session
}

// GetDataBase - get mongodb *mgo.Database
func GetDataBase() *mgo.Database {
	return database
}
