package main

import (
	"rockme_bolt_user_balance_upgrade/db"
	"rockme_bolt_user_balance_upgrade/pkg/config"
	"rockme_bolt_user_balance_upgrade/pkg/utl"
	"time"
)

func init() {
	db.InitializesMongo()
}

func main() {
	config.Lgentry.Infof("Start remittance all user!~~~~~")
	// get all user address
	addresses := utl.GetUserAddress()
	// check no duplicate item
	addresses = utl.RemoveDuplicateElement(addresses)

	// get old bolt balance
	balances := map[string]string{}
	for _, item := range addresses {
		time.Sleep(100 * time.Millisecond)
		balances[item] = utl.GetBalance(item, "OLD")
	}

	// remittance
	for a, b := range balances {
		time.Sleep(200 * time.Millisecond)
		utl.Remittance(a, b)
	}
	config.Lgentry.Infof("Remittance all user Success!~~~~~")

	config.Lgentry.Infof("Start remittance all vendor!~~~~~")
	// get exist vendorID
	vendors, _ := utl.GetVendorIDsAndBalance()

	// remittance vendor
	utl.RemittanceVendorAccount(&vendors)

	for _, i := range vendors {
		config.Lgentry.Infof("vendors: %+v", i)
		balances[i.NewAddress] = i.Balance
	}
	config.Lgentry.Infof("Remittance all vendor success!~~~~~")

	config.Lgentry.Infof("Check all remittance are success!~~~~~")
	for a, b := range balances {
		time.Sleep(100 * time.Millisecond)
		utl.CheckRemittance(a, b)
	}
	config.Lgentry.Infof("All remittance are success!~~~~~")
}
