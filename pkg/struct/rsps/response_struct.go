package rsps

type PingpayGetTWBalanceResponse struct {
	Result  int                 `json:"result"`
	Message string              `json:"message"`
	Data    PingpayGetTWBalance `json:"data"`
}

type PingpayGetTWBalance struct {
	Balance string `json:"balance"`
	Unit    string `json:"unit"`
}

type CreateVendorIDResponse struct {
	Result  int                `json:"result"`
	Message string             `json:"message"`
	Data    CreateVendorIDData `json:"data"`
}

type CreateVendorIDData struct {
	Address string `json:"address"`
}

type RemittanceResponse struct {
	Result  int                  `json:"result"`
	Message string               `json:"message"`
	Data    RemittanceDetailData `json:"data"`
}

type RemittanceDetailData struct {
	LightTxHash string              `json:"lightTxHash"`
	From        string              `json:"from"`
	To          string              `json:"to"`
	LightTxData LightTx             `json:"lightTxData"`
	ReceiptData Receipt             `json:"receiptData"`
	Metadata    RemittanceMetadata  `json:"metadata"`
	ExtraInfo   RemittanceExtraInfo `json:"extraInfo"`
	Ctime       string              `json:"ctime"`
	Utime       string              `json:"utime"`
	UUID        string              `json:"uuid"`
}

type LightTx struct {
	From               string `json:"from"`
	To                 string `json:"to"`
	AssetID            string `json:"assetID"`
	Value              string `json:"value"`
	Fee                string `json:"fee"`
	Nonce              string `json:"nonce"`
	LogID              string `json:"logID"`
	ClientMetadataHash string `json:"clientMetadataHash"`
}

type Receipt struct {
	StageHeight        string `json:"stageHeight"`
	GSN                string `json:"GSN"`
	LightTxHash        string `json:"lightTxHash"`
	FromBalance        string `json:"fromBalance"`
	ToBalance          string `json:"toBalance"`
	ServerMetadataHash string `json:"serverMetadataHash"`
}

type RemittanceMetadata struct {
	Client string `json:"client"`
	Server string `json:"server"`
}

type RemittanceExtraInfo struct {
	TokenType string `json:"tokenType"`
}

type GetBoltAddressResponse struct {
	Result  int                `json:"result"`
	Message string             `json:"message"`
	Data    GetBoltAddressData `json:"data"`
}

type GetBoltAddressData struct {
	Address string `json:"address"`
}
