package rqst

type RemittanceRequest struct {
	TokenType string `json:"tokenType"`
	Value     string `json:"value"`
}
