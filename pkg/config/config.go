package config

import (
	"bufio"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	PublicKey *rsa.PublicKey

	// Log - user logger
	Log *logrus.Logger

	// Lgentry - user logger entry
	Lgentry *logrus.Entry
)

func init() {

	viper.SetConfigType("yaml")
	viper.AddConfigPath("/go/src/rockme_bolt_user_balance_upgrade/")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	rsaKeyPath := viper.GetString("api_key_path")
	publicKeyFile, pubErr := os.Open(rsaKeyPath)
	if pubErr != nil {
		panic("failed to parse public key, Msg:" + pubErr.Error())
	}
	pemfileinfo, _ := publicKeyFile.Stat()
	var size int64 = pemfileinfo.Size()
	pembytes := make([]byte, size)
	buffer := bufio.NewReader(publicKeyFile)
	_, pubErr = buffer.Read(pembytes)
	if pubErr != nil {
		panic("failed to parse public key, Msg:" + pubErr.Error())
	}
	data, _ := pem.Decode([]byte(pembytes))
	publicKeyFile.Close()

	publicKeyInterface, _ := x509.ParsePKIXPublicKey(data.Bytes)
	if publicKeyInterface == nil {
		panic("failed to parse public key, Msg: x509.ParsePKIXPublicKey parse publickey is nil")
	}
	PublicKey = publicKeyInterface.(*rsa.PublicKey)

	Log = logrus.New()
	// Log.SetLevel(Log.InfoLevel)
	Log.SetFormatter(&logrus.TextFormatter{FullTimestamp: true, ForceColors: false})
	Log.Out = os.Stdout
	Lgentry = Log.WithFields(logrus.Fields{})

	Lgentry.Info("init confing")
}
