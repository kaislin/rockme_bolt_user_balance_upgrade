package utl

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"rockme_bolt_user_balance_upgrade/pkg/config"
	"strconv"
	"strings"
	"time"
)

func SignatureMessage() string {
	secretKey := []byte(strconv.FormatInt(time.Now().Unix(), 10))

	encryptedData, err := rsa.EncryptPKCS1v15(rand.Reader, config.PublicKey, secretKey)

	if err != nil {
		config.Lgentry.Errorf("SignatureMessage err: %v\n", err)
	}

	return base64.URLEncoding.EncodeToString(encryptedData)
}

func weiStringToEthString(wei string) string {
	weiInt, _ := strconv.ParseFloat(wei, 64)
	ethInt := weiInt / 1000000000000000000
	return strconv.FormatFloat(ethInt, 'G', -1, 64)
}

func RequestToBOLT(requestURL string, requestMode string, requestBody interface{}, responseBody interface{}) error {
	// request to pinpay to get balance
	encrypted := SignatureMessage()
	var jsonByte []byte
	var resp *http.Response
	var err error
	if strings.ToUpper(requestMode) != "GET" {
		jsonByte, _ = json.Marshal(requestBody)
	}
	if strings.Contains(requestURL, "?") {
		requestURL = requestURL + "&apiKey=" + encrypted
	} else {
		requestURL = requestURL + "?apiKey=" + encrypted
	}

	client := &http.Client{Timeout: time.Duration(5) * time.Second}
	switch requestMode {
	case "POST":
		resp, err = client.Post(requestURL, "application/json", strings.NewReader(string(jsonByte)))
	case "GET":
		resp, err = client.Get(requestURL)
	case "PUT":
		req, _ := http.NewRequest(http.MethodPut, requestURL, strings.NewReader(string(jsonByte)))
		resp, err = client.Do(req)
	}

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&responseBody)
	if err != nil {
		return err
	}
	return nil
}
