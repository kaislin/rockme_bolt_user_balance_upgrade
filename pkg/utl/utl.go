package utl

import (
	"net/url"
	"rockme_bolt_user_balance_upgrade/db"
	"rockme_bolt_user_balance_upgrade/db/model"
	"rockme_bolt_user_balance_upgrade/pkg/config"
	"rockme_bolt_user_balance_upgrade/pkg/struct/rqst"
	"rockme_bolt_user_balance_upgrade/pkg/struct/rsps"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/spf13/viper"
)

var tb db.DBTableOp

func GetUserAddress() (result []string) {
	tb = &db.DBTable{MyDB: db.GetDataBase()}

	// coin_list
	coin := []model.CoinList{}
	err := tb.FindAll("coin_list", nil, &coin)
	if err != nil {
		config.Lgentry.Errorf("get coin_list error: %v", err.Error())
		return
	}

	for _, item := range coin {
		if item.UserAddress != "" && !SearchInSlice(result, item.UserAddress) {
			result = append(result, item.UserAddress)
		}
	}

	// redeem_code
	redeems := []model.RedeemCodeData{}
	err = tb.FindAll("redeem_code", nil, &redeems)
	if err != nil {
		config.Lgentry.Errorf("get redeem_code error: %v", err.Error())
		return
	}

	for _, item := range redeems {
		if item.RedeemAddr != "" && !SearchInSlice(result, item.RedeemAddr) {
			result = append(result, item.RedeemAddr)
		}
	}

	return result
}

func GetVendorIDsAndBalance() (result []model.VendorAddr, err error) {
	// vendor_list all vendorID
	tb = &db.DBTable{MyDB: db.GetDataBase()}
	vendors := []model.VendorList{}
	err = tb.FindAll("vendor_list", nil, &vendors)
	if err != nil {
		config.Lgentry.Errorf("get vendor_list error: %v", err.Error())
		return []model.VendorAddr{}, nil
	}

	// get TideReservoir vendorID
	store := model.StoreList{}
	err = tb.FindOne("store_list", nil, &store)
	if err != nil {
		config.Lgentry.Errorf("get store_list error: %v", err.Error())
		return []model.VendorAddr{}, nil
	}

	vendors = append(vendors, model.VendorList{
		VendorName:    "TideReservoir",
		VendorAddress: store.Address,
	})

	for _, item := range vendors {
		if item.VendorAddress != "" {
			// get new old address
			time.Sleep(100 * time.Millisecond)
			getAddrAPI := viper.GetString("bolt.old_ip") + viper.GetString("bolt.get_addr") + "/" + url.QueryEscape(item.VendorName) + "?tokenType=" + model.COIN
			var getAddr rsps.GetBoltAddressResponse
			RequestToBOLT(getAddrAPI, "GET", nil, &getAddr)

			if getAddr.Message != "" || getAddr.Result == 0 {
				config.Lgentry.Errorf("get new vendorID address %+v error: %v", model.VendorAddr{
					VendorName: url.QueryEscape(item.VendorName),
					OldAddress: "",
					NewAddress: item.VendorAddress,
					Balance:    GetBalance(item.VendorAddress, "OLD"),
				}, getAddr.Message)
				return []model.VendorAddr{}, nil
			}

			time.Sleep(100 * time.Millisecond)
			result = append(result, model.VendorAddr{
				VendorName: url.QueryEscape(item.VendorName),
				OldAddress: getAddr.Data.Address,
				NewAddress: item.VendorAddress,
				Balance:    GetBalance(getAddr.Data.Address, "OLD"),
			})
		}
	}

	return result, nil
}

func GetBalance(userAddr, boltV string) string {
	userAddr = strings.ToLower(userAddr)
	getBalanceAPI := ""
	if boltV == "NEW" {
		getBalanceAPI = viper.GetString("bolt.new_ip") + viper.GetString("bolt.balance") + "/" + userAddr + "?tokenType=" + model.COIN
	} else {
		getBalanceAPI = viper.GetString("bolt.old_ip") + viper.GetString("bolt.balance") + "/" + userAddr + "?tokenType=" + model.COIN
	}
	var result rsps.PingpayGetTWBalanceResponse

	RequestToBOLT(getBalanceAPI, "GET", nil, &result)

	if result.Message != "" || result.Result == 0 {
		config.Lgentry.Errorf("userAddr %v get user balance error: %v", userAddr, result.Message)
		return ""
	}

	return weiStringToEthString(result.Data.Balance)
}

func Remittance(userAddr, balance string) {
	// Remittance
	userAddr = strings.ToLower(userAddr)

	// if address is exist(balance != 0), should not remittance it
	if b := GetBalance(userAddr, "NEW"); b != "" || b != "0" {
		config.Lgentry.Errorf("userAddr(%v) remittance %v not do, because this address is have %v", userAddr, balance, b)
		return
	}

	updateBalanceAPI := viper.GetString("bolt.new_ip") + viper.GetString("bolt.remittance") + "/" + userAddr
	pinpayRequest := rqst.RemittanceRequest{TokenType: model.COIN, Value: balance}

	var remittanceResponse rsps.RemittanceResponse
	RequestToBOLT(updateBalanceAPI, "POST", pinpayRequest, &remittanceResponse)

	if remittanceResponse.Message != "" || remittanceResponse.Result == 0 {
		config.Lgentry.Errorf("userAddr %v remittance %v error: %v", userAddr, balance, remittanceResponse.Message)
		return
	}
	config.Lgentry.Infof("userAddr %v remittance %v Success", userAddr, balance)

	// insert log int transaction_logs
	fbalance, err := strconv.ParseFloat(balance, 64)
	if err != nil {
		config.Lgentry.Errorf("userAddr %v parse balance(string): %v to folat64 err: %v", userAddr, balance, err)
	}
	tb = &db.DBTable{MyDB: db.GetDataBase()}
	data := bson.M{
		"type":             "remittance",
		"from_addr":        "bot_upgrade",
		"to_addr":          userAddr,
		"ad_coin":          fbalance,
		"goods_id":         bson.ObjectIdHex("000000000000000000000000"),
		"light_tx_hash":    remittanceResponse.Data.LightTxHash,
		"ad_source_id":     bson.ObjectIdHex("000000000000000000000000"),
		"redeem_code":      "",
		"transaction_time": time.Now().Unix(),
	}
	err = tb.Insert("transaction_logs", data)
	if err != nil {
		config.Lgentry.Errorf("insert transaction_logs error: %v", err.Error())
		return
	}
}

func RemittanceVendorAccount(vendors *[]model.VendorAddr) {
	for index, _ := range *vendors {
		time.Sleep(200 * time.Millisecond)
		Remittance((*vendors)[index].NewAddress, (*vendors)[index].Balance)
	}
}

func CheckRemittance(userAddr, balance string) {
	if getBalance := GetBalance(userAddr, "NEW"); getBalance != balance {
		config.Lgentry.Errorf("new userAddr(%v) GetBalance is %v != new balance(%v)", userAddr, getBalance, balance)
	}
}

func SearchInSlice(a []string, x string) bool {
	sort.Sort(sort.StringSlice(a))
	index := sort.StringSlice(a).Search(x)
	if index < len(a) && a[index] == x {
		return true
	}
	return false
}

func RemoveDuplicateElement(addrs []string) []string {
	result := make([]string, 0, len(addrs))
	temp := map[string]struct{}{}
	for _, item := range addrs {
		if _, ok := temp[item]; !ok {
			temp[item] = struct{}{}
			result = append(result, item)
		}
	}
	return result
}
